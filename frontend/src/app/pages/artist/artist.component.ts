import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Album } from '../../models/album.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { fetchAlbumsRequest } from '../../store/albums.actions';
import { ActivatedRoute, Params } from '@angular/router';
import { Artist } from '../../models/artist.model';
import { fetchArtistRequest } from '../../store/artists.actions';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.sass']
})
export class ArtistComponent implements OnInit {
  albums: Observable<Album[]>
  artist: Observable<Artist>
  albumsLoading: Observable<boolean>
  albumsError: Observable<null | string>

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute
  ) {
    this.albums = store.select(state => state.albums.albums);
    this.albumsLoading = store.select(state => state.albums.fetchLoading);
    this.albumsError = store.select(state => state.albums.fetchError);

    this.artist = store.select(state => state.artist.artist);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.store.dispatch(fetchArtistRequest({id: params['id']}));
      this.store.dispatch(fetchAlbumsRequest({id: params['id']}));
    });
  }

}
