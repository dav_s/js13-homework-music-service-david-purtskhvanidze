import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Params } from '@angular/router';
import { Track } from '../../models/track.model';
import { fetchTracksRequest } from '../../store/tracks.actions';
import { fetchAlbumsRequest } from '../../store/albums.actions';

@Component({
  selector: 'app-tracks',
  templateUrl: './tracks.component.html',
  styleUrls: ['./tracks.component.sass']
})
export class TracksComponent implements OnInit {
  tracks: Observable<Track[]>
  tracksLoading: Observable<boolean>
  tracksError: Observable<null | string>

  constructor(
    private store: Store<AppState>,
    private route: ActivatedRoute
  ) {
    this.tracks = store.select(state => state.tracks.tracks);
    this.tracksLoading = store.select(state => state.tracks.fetchLoading);
    this.tracksError = store.select(state => state.tracks.fetchError);
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.store.dispatch(fetchTracksRequest({id: params['id']}));
    });
  }

}
