import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { RegisterError } from '../../models/user.model';
import { Observable } from 'rxjs';
import { ArtistData } from '../../models/artist.model';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { addArtistRequest } from '../../store/artists.actions';

@Component({
  selector: 'app-add-artist',
  templateUrl: './add-artist.component.html',
  styleUrls: ['./add-artist.component.sass']
})
export class AddArtistComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<string | null>;

  constructor(
    private store: Store<AppState>
  ) {
    this.loading = store.select(state => state.artist.fetchLoading);
    this.error = store.select(state => state.artist.fetchError);
  }

  ngOnInit(): void {
    //this.store.dispatch(fetchCategoriesRequest());
  }

  onSubmit() {
    const artistData: ArtistData = this.form.value;
    this.store.dispatch(addArtistRequest({artistData}));
  }
}
