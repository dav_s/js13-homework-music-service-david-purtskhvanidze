import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ArtistComponent } from './pages/artist/artist.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { TracksComponent } from './pages/tracks/tracks.component';
import { AddArtistComponent } from './pages/add-artist/add-artist.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'artist/:id', component: ArtistComponent},
  {path: 'artist/:id', component: ArtistComponent},
  {path: 'add-artist', component: AddArtistComponent},
  {path: 'album/:id', component: TracksComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login', component: LoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
