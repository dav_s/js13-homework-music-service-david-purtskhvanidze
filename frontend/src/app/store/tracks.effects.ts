import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  fetchTracksRequest,
  fetchTracksSuccess,
  fetchTracksFailure,
} from './tracks.actions';
import { mergeMap, catchError, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { TracksService } from '../services/tracks.service';

@Injectable()
export class TracksEffects {

  constructor(
    private actions: Actions,
    private tracksService: TracksService,
  ) {}

  fetchTracks = createEffect(() => this.actions.pipe(
    ofType(fetchTracksRequest),
    mergeMap(id => this.tracksService.getTracksByAlbum(id.id).pipe(
      map(tracks => fetchTracksSuccess({tracks})),
      catchError(() => of(fetchTracksFailure({
        error: 'Something went wrong'
      })))
    ))
  ));
}
