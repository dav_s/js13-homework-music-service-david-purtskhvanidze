import { createAction, props } from '@ngrx/store';
import { Artist, ArtistData } from '../models/artist.model';

export const fetchArtistsRequest = createAction('[Artists] Fetch Request');
export const fetchArtistsSuccess = createAction('[Artists] Fetch Success', props<{artists: Artist[]}>());
export const fetchArtistsFailure = createAction('[Artists] Fetch Failure', props<{error: string}>());

export const fetchArtistRequest = createAction('[Artist] Fetch Request', props<{id: string}>());
export const fetchArtistSuccess = createAction('[Artist] Fetch Success', props<{artist: Artist}>());
export const fetchArtistFailure = createAction('[Artist] Fetch Failure', props<{error: string}>());

export const addArtistRequest = createAction('[Artist] Add Request', props<{artistData: ArtistData}>());
export const addArtistSuccess = createAction('[Artist] Add Success');
export const addArtistFailure = createAction('[Artist] Add Failure', props<{error: string}>());
