import { Artist } from '../models/artist.model';
import { Album } from '../models/album.model';
import { LoginError, RegisterError, User } from '../models/user.model';
import { Track } from '../models/track.model';

export type ArtistsState = {
  artists: Artist[],
  artist: Artist,
  fetchLoading: boolean,
  fetchError: null | string,
};

export type ArtistState = {
  artists: Artist[],
  artist: Artist,
  fetchLoading: boolean,
  fetchError: null | string,
};

export type AlbumsState = {
  albums: Album[],
  artist: Artist,
  fetchLoading: boolean,
  fetchError: null | string,
};

export type TracksState = {
  tracks: Track[],
  fetchLoading: boolean,
  fetchError: null | string,
};

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError,
}

export type AppState = {
  artists: ArtistsState,
  artist: ArtistState,
  albums: AlbumsState,
  tracks: TracksState,
  users: UsersState,
}
