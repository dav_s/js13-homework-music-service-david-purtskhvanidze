import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AlbumsService } from '../services/albums.service';
import {
  fetchAlbumsRequest,
  fetchAlbumsSuccess,
  fetchAlbumsFailure,
} from './albums.actions';
import { mergeMap, catchError, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AlbumsEffects {

  constructor(
    private actions: Actions,
    private albumsService: AlbumsService,
  ) {}

  fetchAlbums = createEffect(() => this.actions.pipe(
    ofType(fetchAlbumsRequest),
    mergeMap(id => this.albumsService.getAlbumsByArtist(id.id).pipe(
      map(albums => fetchAlbumsSuccess({albums})),
      catchError(() => of(fetchAlbumsFailure({
        error: 'Something went wrong'
      })))
    ))
  ));
}
