import { createReducer, on } from '@ngrx/store';
import {
  fetchAlbumsRequest,
  fetchAlbumsSuccess,
  fetchAlbumsFailure,
} from './albums.actions';
import { AlbumsState } from './types';
import { Artist } from '../models/artist.model';

const initialState: AlbumsState = {
  albums: [],
  artist: <Artist>{},
  fetchLoading: false,
  fetchError: null,
};

export const albumsReducer = createReducer(
  initialState,
  on(fetchAlbumsRequest, state => ({...state, fetchLoading: true})),
  on(fetchAlbumsSuccess, (state, {albums}) => ({
    ...state,
    fetchLoading: false,
    albums
  })),
  on(fetchAlbumsFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  }))
);
