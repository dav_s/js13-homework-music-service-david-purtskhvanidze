import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ArtistsService } from '../services/artists.service';
import {
  fetchArtistsRequest,
  fetchArtistsSuccess,
  fetchArtistsFailure,
  fetchArtistSuccess,
  fetchArtistFailure,
  fetchArtistRequest, addArtistRequest, addArtistFailure, addArtistSuccess
} from './artists.actions';
import { mergeMap, catchError, of, withLatestFrom } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from './types';
import { logoutUserRequest, registerUserFailure, registerUserRequest, registerUserSuccess } from './users.actions';
import { Router } from '@angular/router';
import { HelpersService } from '../services/helpers.service';

@Injectable()
export class ArtistsEffects {

  constructor(
    private actions: Actions,
    private artistsService: ArtistsService,
    private store: Store<AppState>,
    private router: Router,
    private helpers: HelpersService,
  ) {}

  addArtists = createEffect(() => this.actions.pipe(
    ofType(addArtistRequest),
    withLatestFrom(this.store.select(state => state.users.user)),
    mergeMap(([{artistData}, user]) => this.artistsService.addArtist(artistData, user!.token).pipe(
      map(artist => addArtistSuccess()),
      tap(() => {
        this.helpers.openSnackbar('Register successful');
        void this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(addArtistFailure)
    ))
  ));

  fetchArtists = createEffect(() => this.actions.pipe(
    ofType(fetchArtistsRequest),
    mergeMap(() => this.artistsService.getArtists().pipe(
      map(artists => fetchArtistsSuccess({artists})),
      catchError(() => of(fetchArtistsFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  fetchArtist = createEffect(() => this.actions.pipe(
    ofType(fetchArtistRequest),
    mergeMap(id => this.artistsService.getArtist(id.id).pipe(
      map(artist => fetchArtistSuccess({artist})),
      catchError(() => of(fetchArtistFailure({
        error: 'Something went wrong'
      })))
    ))
  ));
}
