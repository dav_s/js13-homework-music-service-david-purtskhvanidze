import { createReducer, on } from '@ngrx/store';
import {
  fetchTracksRequest,
  fetchTracksSuccess,
  fetchTracksFailure
} from './tracks.actions';
import { TracksState } from './types';

const initialState: TracksState = {
  tracks: [],
  fetchLoading: false,
  fetchError: null,
};

export const tracksReducer = createReducer(
  initialState,
  on(fetchTracksRequest, state => ({...state, fetchLoading: true})),
  on(fetchTracksSuccess, (state, {tracks}) => ({
    ...state,
    fetchLoading: false,
    tracks
  })),
  on(fetchTracksFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  }))
);
