import { createReducer, on } from '@ngrx/store';
import {
  fetchArtistsRequest,
  fetchArtistsSuccess,
  fetchArtistsFailure,
  fetchArtistRequest,
  fetchArtistSuccess,
  fetchArtistFailure,
  addArtistRequest,
  addArtistSuccess,
  addArtistFailure,

} from './artists.actions';
import { ArtistsState } from './types';
import { Artist } from '../models/artist.model';

const initialState: ArtistsState = {
  artists: [],
  artist: <Artist>{},
  fetchLoading: false,
  fetchError: null,
};

export const artistsReducer = createReducer(
  initialState,
  on(fetchArtistsRequest, state => ({...state, fetchLoading: true})),
  on(fetchArtistsSuccess, (state, {artists}) => ({
    ...state,
    fetchLoading: false,
    artists
  })),
  on(fetchArtistsFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  }))
);

export const artistReducer = createReducer(
  initialState,
  on(fetchArtistRequest, state => ({...state, fetchLoading: true})),
  on(fetchArtistSuccess, (state, {artist}) => ({
    ...state,
    fetchLoading: false,
    artist
  })),
  on(fetchArtistFailure, (state, {error}) => ({
    ...state,
    fetchLoading: false,
    fetchError: error
  })),
  on(addArtistRequest, state => ({...state, addLoading: true})),
  on(addArtistSuccess, (state) => ({
    ...state,
    addLoading: false,
  })),
  on(addArtistFailure, (state, {error}) => ({
    ...state,
    addLoading: false,
    addError: error
  }))
);
