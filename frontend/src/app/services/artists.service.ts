import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiArtistData, Artist, ArtistData } from '../models/artist.model';
import { map } from 'rxjs/operators';
import { environment as env, environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ArtistsService {

  constructor(private http: HttpClient) { }

  getArtists() {
    return this.http.get<ApiArtistData[]>(environment.apiUrl + '/artists').pipe(
      map(response => {
        return response.map(artistData => {
          return new Artist(
            artistData._id,
            artistData.title,
            artistData.description,
            artistData.image,
            artistData.is_published
          );
        });
      })
    );
  }

  getArtist(id: string) {
    return this.http.get<Artist>(environment.apiUrl + '/artists/' + id);
  }

  addArtist(artistData: ArtistData, token: string) {

    const formData = new FormData();
    Object.keys(artistData).forEach(key => {
      if (artistData[key] !== null) {
        formData.append(key, artistData[key]);
      }
    });

    console.log(token, formData);
    return this.http.post<Artist>(env.apiUrl + '/artists', formData, {
      headers: new HttpHeaders({'Authorization': token})
    });
  }
}
