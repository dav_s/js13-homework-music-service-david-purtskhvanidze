import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Album, AlbumsWithArtist, ApiAlbumData, ApiAlbumsWithArtistData } from '../models/album.model';
import { Artist } from '../models/artist.model';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {

  constructor(
    private http: HttpClient,
  ) { }

  getAlbumsByArtist(id: string) {
    return this.http.get<ApiAlbumData[]>(environment.apiUrl + '/albums?artist=' + id).pipe(
      map(response => {
        return response.map(albumData => {
          const albums = new Album(
            albumData._id,
            albumData.title,
            albumData.artist,
            albumData.release,
            albumData.image,
          );
          return albums;
        });
      })
    );
  }

  getAlbumsWithArtist(id: string) {
    return this.http.get<ApiAlbumsWithArtistData>(environment.apiUrl + '/albums/withArtist/' + id).pipe(
      map(albumsWithArtistData => {
        const albumsWithArtist = new AlbumsWithArtist(
          albumsWithArtistData.albums,
          albumsWithArtistData.artist,
        );
        return albumsWithArtist;
      })
    );
  }
}
