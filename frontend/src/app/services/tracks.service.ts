import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { ApiTrackData, Track } from '../models/track.model';
import { Album, ApiAlbumData } from '../models/album.model';

@Injectable({
  providedIn: 'root'
})
export class TracksService {

  constructor(private http: HttpClient) { }

  getTracks() {
    return this.http.get<ApiTrackData[]>(environment.apiUrl + '/tracks').pipe(
      map(response => {
        return response.map(trackData => {
          return new Track(
            trackData._id,
            trackData.title,
            trackData.album,
            trackData.duration,
          );
        });
      })
    );
  }

  getTracksByAlbum(id: string) {
    return this.http.get<ApiTrackData[]>(environment.apiUrl + '/tracks?album=' + id).pipe(
      map(response => {
        return response.map(trackData => {
          return new Track(
            trackData._id,
            trackData.title,
            trackData.album,
            trackData.duration,
          );
        });
      })
    );
  }
}
