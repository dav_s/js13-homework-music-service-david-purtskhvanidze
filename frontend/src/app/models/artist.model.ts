export class Artist {
  constructor(
    public id: string,
    public title: string,
    public description: string,
    public image: string,
    public is_published: boolean,
  ) {}
}

export interface AddArtistData {
  title: string,
  description: string,
  image: File | null,
  is_published: false
}

export interface ArtistData {
  [key: string]: any,
  title: string,
  description: string,
  image: File | null,
  is_published: boolean,
}

export interface ApiArtistData {
  _id: string,
  title: string,
  description: string,
  image: string,
  is_published: boolean,
}
