export class Track {
  constructor(
    public id: string,
    public title: string,
    public album: string,
    public duration: string,
  ) {}
}

export interface TrackData {
  [key: string]: any;
  title: string;
  album: string;
  duration: string;
}

export interface ApiTrackData {
  _id: string,
  title: string;
  album: string;
  duration: string;
}
