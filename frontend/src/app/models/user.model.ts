export interface User {
  _id: string,
  email: string,
  displayName: string,
  image: string,
  token: string,
}

export interface RegisterUserData {
  email: string,
  displayName: string,
  image: File | null,
  password: string
}


export interface LoginUserData {
  email: string,
  password: string,
}

export interface FieldError {
  message: string
}

export interface RegisterError {
  errors: {
    email: FieldError,
    displayName: FieldError,
    image: FieldError,
    password: FieldError,
  }
}

export interface LoginError {
  error: string
}
