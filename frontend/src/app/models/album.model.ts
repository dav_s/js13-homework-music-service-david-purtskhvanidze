export class Album {
  constructor(
    public id: string,
    public title: string,
    public artist: {
      title: string,
      description: string
    },
    public release: string,
    public image: string,
  ) {}
}
export class AlbumsWithArtist {
  constructor(
    public albums: {
      _id: string,
      title: string,
      artist: {
        _id: string,
        title: string,
        description: string
      },
      release: string,
      image: string,
    },
    public artist: {
      _id: string,
      title: string,
      description: string
    },
  ) {}
}

export interface AlbumData {
  [key: string]: any;
  title: string;
  artist: {
    title: string,
    description: string
  },
  release: string;
  image: File | null;
}

export interface AlbumsWithArtistData {
  albums: {
    _id: string,
    title: string;
    artist: {
      _id: string,
      title: string,
      description: string
    },
    release: string;
    image: File | null;
  },
  artist: {
    _id: string,
    title: string,
    description: string
  },
}

export interface ApiAlbumData {
  _id: string,
  title: string;
  artist: {
    title: string,
    description: string
  },
  release: string;
  image: string;
}

export interface ApiAlbumsWithArtistData {
  albums: {
    _id: string,
    title: string;
    artist: {
      _id: string,
      title: string,
      description: string
    },
    release: string;
    image: string;
  },
  artist: {
    _id: string,
    title: string,
    description: string
  },
}
