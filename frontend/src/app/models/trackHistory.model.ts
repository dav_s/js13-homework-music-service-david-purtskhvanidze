export class trackHistory {
  constructor(
    public id: string,
    public user: string,
    public track: string,
    public datetime: string,
  ) {}
}

export interface trackHistoryData {
  [key: string]: any;
  user: string;
  track: string;
  datetime: string;
}

export interface ApiTrackHistoryData {
  _id: string,
  user: string;
  track: string;
  datetime: string;
}
