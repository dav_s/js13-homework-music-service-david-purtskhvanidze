const mongoose = require('mongoose');

const ArtistSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    description: String,
    image: String,
    is_published: {
        type: Boolean,
        required: true,
        default: false,
    },
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;