const express = require('express');
const Track = require('../models/Track');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

router.get("/", async (req, res, next) => {
    try {
        const query = {};

        if (req.query.album) {
            query.album = req.query.album;
        }

        const tracks = await Track.find(query);
        return res.send(tracks);
    } catch(e) {
        next(e);
    }
});


router.post('/', auth, async (req, res, next) => {
    try {
        const trackData = {
            title: req.body.title,
            album: req.body.album,
            duration: req.body.duration,
            is_published: false
        };


        if (req.user.role === 'admin') {
            trackData.is_published = true;
        }

        const track = new Track(trackData);
        await track.save();

        return res.send({message: 'Created new track', id: track._id});
    } catch (e) {
        next(e);
    }
});

module.exports = router;
