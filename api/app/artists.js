const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const Artist = require('../models/Artist');
const auth = require("../middleware/auth");
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get("/", async (req, res, next) => {
    try {
        const artists = await Artist.find();
        return res.send(artists);
    } catch(e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const artist = await Artist.findById(req.params.id);

        if (!artist) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(artist);
    } catch (e) {
        next(e);
    }
});

router.post('/', auth, upload.single('image'), async (req, res, next) => {
    try {
        const artistData = {
            title: req.body.title,
            description: req.body.description,
            image: null,
            is_published: false
        };

        if (req.file) {
            artistData.image = req.file.filename;
        }

        if (req.user.role === 'admin') {
            artistData.is_published = true;
        }

        const artist = new Artist(artistData);
        await artist.save();

        return res.send({message: 'Created new artist', id: artist._id});
    } catch (e) {
        next(e);
    }
});

module.exports = router;
