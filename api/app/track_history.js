const express = require('express');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const TrackHistory = require('../models/TrackHistory');

const router = express.Router();

router.post('/', auth, permit('admin'), async (req, res, next) => {
    try {
        const TrackHistoryData = {
            user: req.user,
            track: req.body.track,
            datetime: req.body.datetime
        };

        const trackHistory = new TrackHistory(TrackHistoryData);
        await trackHistory.save();

        return res.send({message: 'Created new track history', id: trackHistory._id});
    } catch (e) {
        next(e);
    }
});

module.exports = router;
