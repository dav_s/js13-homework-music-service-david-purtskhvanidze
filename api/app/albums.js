const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const Album = require('../models/Album');
const Artist = require('../models/Artist');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get("/", async (req, res, next) => {
    try {
        const query = {};

        if (req.query.artist) {
            query.artist = req.query.artist;
        }

        const albums = await Album.find(query).populate('artist');
        return res.send(albums);

    } catch(e) {
        next(e);
    }
});

router.get("/withArtist/:id", async (req, res, next) => {
    try {
        const albums = await Album.find({artist: req.params.id}).populate('artist');
        const artist = await Artist.findById(req.params.id);

        return res.send({albums: albums, artist: artist});
    } catch(e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const album = await Album.findById(req.params.id).populate('artist');

        if (!album) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(album);
    } catch (e) {
        next(e);
    }
});

router.post("/", auth, upload.single('image'), async (req, res, next) => {
    try {
        const albumData = {
            title: req.body.title,
            artist: req.body.artist,
            release: req.body.release,
            image: null,
            is_published: false,
        };

        if (req.file) {
            albumData.image = req.file.filename;
        }

        if (req.user.role === 'admin') {
            albumData.is_published = true;
        }

        const album = new Album(albumData);

        await album.save();

        return res.send(album);
    } catch(e) {
        next(e);
    }
});

module.exports = router;
