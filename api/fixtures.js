const mongoose = require('mongoose');
const config = require("./config");
const Album = require('./models/Album');
const Artist = require('./models/Artist');
const Track = require("./models/Track");
const User = require("./models/User");
const {nanoid} = require("nanoid");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }


  const [artist1, artist2, artist3] = await Artist.create({
    title: 'Artist1',
    description: 'Artist1 description',
    image: 'Artist1.jpeg'
  }, {
    title: 'Artist2',
    description: 'Artist2 description',
    image: 'Artist2.jpeg'
  }, {
    title: 'Artist3',
    description: 'Artist3 description',
    image: 'Artist3.jpeg'
  });

  const [artist1Album1, artist1Album2, artist2Album1, artist2Album2, artist3Album1] = await Album.create({
    title: 'Artist1 album1',
    artist: artist1,
    release: '12 22 2014',
    image: 'Album1.jpeg'
  }, {
    title: 'Artist1 album2',
    artist: artist1,
    release: '12 33 2020',
    image: 'Album11.jpeg'
  }, {
    title: 'Artist2 album1',
    artist: artist2,
    release: '12 13 2026',
    image: 'Album2.jpeg'
  }, {
    title: 'Artist2 album2',
    artist: artist2,
    release: '12 13 2019',
    image: 'Album22.jpeg'
  }, {
    title: 'Artist3 album1',
    artist: artist3,
    release: '12 14 2050',
    image: 'Album3.jpeg'
  });

  await Track.create({
    title: 'Track1',
    album: artist1Album1,
    duration: '2:20'
  }, {
    title: 'Track2',
    album: artist1Album1,
    duration: '1:20'
  }, {
    title: 'Track3',
    album: artist1Album1,
    duration: '1:50'
  }, {
    title: 'Track4',
    album: artist1Album1,
    duration: '2:33'
  }, {
    title: 'Track5',
    album: artist1Album1,
    duration: '2:13'
  }, {
    title: 'Track1',
    album: artist1Album2,
    duration: '2:13'
  }, {
    title: 'Track2',
    album: artist1Album2,
    duration: '2:11'
  }, {
    title: 'Track3',
    album: artist1Album2,
    duration: '2:22'
  }, {
    title: 'Track4',
    album: artist1Album2,
    duration: '2:15'
  }, {
    title: 'Track5',
    album: artist1Album2,
    duration: '2:17'
  }, {
    title: 'Track1',
    album: artist2Album1,
    duration: '2:11'
  }, {
    title: 'Track2',
    album: artist2Album1,
    duration: '2:18'
  }, {
    title: 'Track3',
    album: artist2Album1,
    duration: '2:34'
  }, {
    title: 'Track4',
    album: artist2Album1,
    duration: '2:33'
  }, {
    title: 'Track5',
    album: artist2Album1,
    duration: '2:22'
  }, {
    title: 'Track1',
    album: artist2Album2,
    duration: '2:45'
  }, {
    title: 'Track2',
    album: artist2Album2,
    duration: '2:12'
  }, {
    title: 'Track3',
    album: artist2Album2,
    duration: '2:30'
  }, {
    title: 'Track4',
    album: artist2Album2,
    duration: '2:66'
  }, {
    title: 'Track5',
    album: artist2Album2,
    duration: '2:34'
  }, {
    title: 'Track1',
    album: artist3Album1,
    duration: '2:11'
  }, {
    title: 'Track2',
    album: artist3Album1,
    duration: '2:24'
  }, {
    title: 'Track3',
    album: artist3Album1,
    duration: '4:34'
  }, {
    title: 'Track4',
    album: artist3Album1,
    duration: '3:34'
  }, {
    title: 'Track5',
    album: artist3Album1,
    duration: '10:34'
  });

  await User.create({
    email: 'admin@admin.com',
    password: '123',
    displayName: 'Admin',
    image: 'Avatar1.jpeg',
    token: nanoid(),
    role: 'admin'
  }, {
    email: 'user@user.com',
    password: '123',
    displayName: 'User',
    image: 'Avatar2.jpeg',
    token: nanoid(),
    role: 'user'
  })

  await mongoose.connection.close();
};

run().catch(e => console.error(e));